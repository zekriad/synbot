const redis = require('../lib/redis');

module.exports = class Currency {
  constructor(user) {
    this.user = user;
    this._total = null;
  }

  async total() {
    if (this._total) {
      return this._total;
    } else {
      return await redis.hget(`users:${this.user.id}`, 'currency');
    }
  }

  async gain(amount) {
    if (amount < 0) return;

    await redis.hincrby(`users:${this.user.id}`, 'currency', amount);
    this._total = null;
    return amount;
  }

  async spend(amount) {
    if (amount < 0 || amount > await this.total()) return;

    await redis.hincrby(`users:${this.user.id}`, 'currency', -amount);
    this._total = null;
    return -amount;
  }

  async give(another, amount) {
    if (amount < 0 || amount > await this.total()) return;

    await redis.multi()
               .hincrby(`users:${another.id}`, 'currency', amount)
               .hincrby(`users:${this.user.id}`, 'currency', -amount)
               .exec();

    this._total = null;
    return amount;
  }
};
