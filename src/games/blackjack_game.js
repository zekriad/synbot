const { shuffle } = require('../support/array_helpers.js');
const Messages = require('./blackjack_messages');

const cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const timeout = 15000;

const { Enum } = require('enumify');
class Mode extends Enum {}
Mode.initEnum(['DUEL', 'CASINO']);

global.blackjackGame = null;

module.exports = class BlackjackGame {
  constructor() {
    this.deck = shuffle([...cards, ...cards, ...cards, ...cards]);
    this.players = [];
    this.position = 0;
    this.gameStarted = true;
    this.inProgress = false;
  }

  join(user) {
    if (!this.isPlayerJoined(user)) {
      this.players.push({
        user: user,
        hand: [this.deck.pop(), this.deck.pop()],
        stand: false,
        active: true,
        lastAction: null
      });
    }
  }

  quit(user) {
    if (this.isPlayerJoined(user)) {
      this.players = this.players.filter(p => p.user !== user);
    }
  }

  isPlayerJoined(user) {
    return this.getPlayerNames().includes(user);
  }

  getPlayer(user) {
    return this.players.filter(p => p.user === user)[0];
  }

  getPlayerNames() {
    return this.players.map(p => p.user);
  }

  getNumberOfPlayers() {
    return this.players.length;
  }

  start(mode = 'DUEL') {
    this.mode = Mode.enumValueOf(mode);
    this.players = shuffle(this.players);

    this.join(Messages.DEALER_NAME);
    this.position = 0;
    this.inProgress = true;
  }

  end() {
    this.gameStarted = false;
    this.inProgress = false;
  }

  nextPlayer() {
    this.position++;
    return this.getCurrentPlayer();
  }

  getPlayerCardDetails(user) {
    const player = this.getPlayer(user);

    if (!player) return;

    return {
      user: player.user,
      hand: player.hand,
      sum: this._getCardsSum(player.hand)
    };
  }

  getAllPlayersCardsDetails() {
    var details = [];
    for (const p of this.players) {
      details.push({ user: p.user, hand: p.hand, sum: this._getCardsSum(p.hand) });
    }
    return details;
  }

  getCurrentPlayer() {
    return this.players[this.position];
  }

  startActiveTimer() {

  }

  updateLastAction(user) {
    this.getPlayer(user).lastAction = new Date().getTime();
  }

  hit(user) {
    const newCard = this.deck.pop();
    this.getPlayer(user).hand.push(newCard);
    this.updateLastAction(user);
    return newCard;
  }

  stand(user) {
    this.getPlayer(user).stand = true;
  }

  dealerAI(user) {
    while (!this.hasBusted(user) && !this.computeWinners().includes(Messages.DEALER_NAME)) {
      this.hit(user);
    }
    this.stand(user);
  }

  hasBlackjack(user) {
    return this._getCardsSum(this.getPlayer(user).hand) === 21;
  }

  hasBusted(user) {
    return this._getCardsSum(this.getPlayer(user).hand) > 21;
  }

  computeWinners() {
    const details = this.getAllPlayersCardsDetails();

    var maxSum = 0;
    details.forEach(detail => {
      if (detail.sum > maxSum && detail.sum <= 21)
        maxSum = detail.sum;
    });

    var sumWinners = details.filter(detail => detail.sum >= maxSum && detail.sum <= 21);

    var minCards = 9999;
    sumWinners.forEach(sumWinner => {
      if (sumWinner.hand.length <= minCards)
        minCards = sumWinner.hand.length;
    });

    return sumWinners.filter(w => w.hand.length <= minCards).map(w => w.user);
  }

  _isLowAceHand(hand) {
    const highAceSum = hand.reduce((acc, card) => acc + this._getCardValue(card), 0);
    return highAceSum <= 21;
  }

  _getCardsSum(hand) {
    const highAceHand = hand.reduce((acc, card) => acc + this._getCardValue(card), 0);
    return highAceHand <= 21 ? highAceHand : hand.reduce((acc, card) => acc + this._getCardValue(card, true), 0);
  }

  _getCardValue(card, isLowAce = false) {
    var value;

    switch (card) {
      case 'J':
      case 'Q':
      case 'K': value = 10; break;
      case 'A': value = isLowAce ? 1 : 11; break;
      default: value = Number(card); break;
    }

    return value;
  }
};
