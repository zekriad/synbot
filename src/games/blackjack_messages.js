const DEALER_NAME = "Dealer";
const ROOM_NAME = "blackjack";

const BLACKJACK_MSG = `Only works in #${ROOM_NAME} - Initialize commands: !init, !players, !join, !quit, !start | In-game commands: !hit, !stand, !status, !end`;
const GAME_ALREADY_STARTED_MSG = "A game has already started!";
const GAME_NOT_STARTED_MSG = "A game hasn't been started yet!";
const GAME_ALREADY_IN_PROGRESS_MSG = "A game is already in progress!";
const GAME_NOT_IN_PROGRESS_MSG = "A game is not in progress!";
const GAME_ENDED_MSG = "The game has ended.";
const NOT_ENOUGH_PLAYERS_MSG = "Not enough players have joined the game.";
const NO_MORE_PLAYERS_MSG = "The game has ended (no more players).";
const NO_WINNERS_MSG = "No winners! You all suck.";

module.exports = class BlackjackMessages {
  static get COMMANDS() { return BLACKJACK_MSG; }
  static get GAME_ALREADY_STARTED() { return GAME_ALREADY_STARTED_MSG; }
  static get GAME_NOT_STARTED() { return GAME_NOT_STARTED_MSG; }
  static get GAME_ALREADY_IN_PROGRESS() { return GAME_ALREADY_IN_PROGRESS_MSG; }
  static get GAME_NOT_IN_PROGRESS() { return GAME_NOT_IN_PROGRESS_MSG; }
  static get GAME_ENDED() { return GAME_ENDED_MSG; }
  static get NOT_ENOUGH_PLAYERS() { return NOT_ENOUGH_PLAYERS_MSG; }
  static get NO_MORE_PLAYERS() { return NO_MORE_PLAYERS_MSG; }
  static get NO_WINNERS() { return NO_WINNERS_MSG; }

  static get DEALER_NAME() { return DEALER_NAME; }
  static get ROOM_NAME() { return ROOM_NAME; }

  static getNewGameMsg(user) {
    return `${user} has started a new game of blackjack.`;
  }

  static getPlayerTurnMsg(user) {
    return `${user}'s turn: What would you like to do?`;
  }

  static getPlayerDetailsMsg(playerDetails) {
    return `${playerDetails.user}'s hand: ${playerDetails.hand.toString().replace(/,/g, ", ")} (${playerDetails.sum})`;
  }

  static getPlayerListMsg(players) {
    return players.toString().replace(/,/g, ", ");
  }

  static getCurrentPlayersListMsg(players) {
    return `Current players: ${this.getPlayerListMsg(players)}`;
  }

  static getAllPlayersDetailsMsg(playerDetails, showHole = false) {
    var msg = "";

    playerDetails.forEach(detail => {
      if (detail.user !== DEALER_NAME) {
        msg += `${detail.user}'s hand: ${detail.hand.toString().replace(/,/g, ", ")} (${detail.sum}) | `;
      } else {
        const dealersHand = detail.hand.map((card, index) => index === 0 || showHole ? card : '?');
        msg = msg.replace(/^/, `${DEALER_NAME}'s hand: ${dealersHand.toString().replace(/,/g, ", ")} ${showHole ? `(${detail.sum})` : ''} | `);
      }
    });
    msg = msg.slice(0, msg.length - 3);

    return msg;
  }
  
  static getWinnersMsg(winners) {
    return winners.length > 0 ? `${winners.length > 1 ? "Winners" : "Winner"}: ${this.getPlayerListMsg(winners)}` : NO_WINNERS_MSG;
  }

  static getWinnerBlackjackMsg(winners) {
    return `Blackjack! ${this.getWinnersMsg(winners)}`;
  }

  static hasBustedMsg(user) {
    return `${user} has busted!`;
  }
};
