exports.isLetter = c => c.toUpperCase() !== c.toLowerCase();
