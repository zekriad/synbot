exports.bold = str => `**${str}**`;
exports.italic = str => `_${str}_`;
exports.underline = str => `__${str}__`;
