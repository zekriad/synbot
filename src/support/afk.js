afkers = {};

this.anyAfkers = () => Object.values(afkers).length > 0;
this.isAfk = user => afkers[user.id];
this.addAfker = user => afkers[user.id] = true;
this.removeAfker = user => delete afkers[user.id];
