const lastMessages = {};

exports.getLast = chan => lastMessages[chan] ? lastMessages[chan].content : '';
exports.setLast = (chan, msg) => lastMessages[chan] = msg;
