const Hook = require('./hook');
const history = require('../support/history');

module.exports = class History extends Hook {
  call() {
    history.setLast(this.message.channel.name, this.message);
  }
};
