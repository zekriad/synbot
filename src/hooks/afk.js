const Hook = require('./hook');
const afk = require('../support/afk');

module.exports = class AFKHook extends Hook {
  call() {
    if (!afk.anyAfkers()) return;

    if (afk.isAfk(this.message.author)) {
      afk.removeAfker(this.message.author);
    }

    if (this.message.mentions.members.length < 1) return;

    let afkers = [];

    this.message
        .mentions
        .members
        .forEach(m => {
          let user = m.user;

          if (afk.isAfk(user)) {
            afkers.push(user.username);
          }
        });

    if (afkers.length > 0) {
      this.message.channel.send(`[${afkers.join(', ')}] are AFK.`);
    }
  }
};
