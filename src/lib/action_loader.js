const fs = require('fs');

module.exports = class CommandLoader {
  constructor(paths) {
    this.paths = paths;
    this.modules = {};
  }

  loadActions() {
    this.paths.forEach(path => {
      this.modules[path] = {};

      [].concat
        .apply([], this.readCommands(`./src/${path}`))
        .map(s => require(`../${path}/${s}`))
        .forEach(m => {
          this.modules[path][m.key] = m;
          if (m.aliases) {
            m.aliases.forEach(al => this.modules[path][al] = m);
          }
        });
    });

    return this.modules;
  }

  loadHooks() {
    return [].concat
             .apply([], this.readCommands(`./src/hooks`))
             .map(s => require(`../hooks/${s}`));
  }

  readCommands(path, prefix = '')  {
    return fs.readdirSync(path, { withFileTypes: true })
             .map(s => {
               if (s.isDirectory()) {
                 return this.readCommands(`${path}/${s.name}`, `${prefix + s.name}/`);
               } else {
                 return `${prefix}${s.name.replace(/\.[^/.]+$/, "")}`;
               }
             });
  }
};
