const redis = require('./redis');

const cachedAdmins = new Set([]);
const cachedBans = new Set([]);

const refreshAdmins = async () => {
  const admins = await redis.smembers('users:admins');
  cachedAdmins.clear();
  admins.forEach(uid => cachedAdmins.add(uid));
}

const refreshBans = async () => {
  const banned = await redis.smembers('users:banned');
  cachedBans.clear();
  banned.forEach(uid => cachedBans.add(uid));
}

const refresh = () => {
  refreshAdmins();
  refreshBans();
}

refresh();


exports.adminList = () => [...cachedAdmins];

exports.banList = () => [...cachedBans];

exports.isAdmin = user => cachedAdmins.has(user.id),

exports.addAdmin = async user => {
  await redis.sadd('users:admins', user.id);
  refresh();
},

exports.removeAdmin = async user => {
  await redis.srem('users:admins', user.id);
  refresh();
},

exports.isBanned = user => cachedBans.has(user.id),

exports.ban = async user => {
  await redis.sadd('users:banned', user.id);
  refresh();
},

exports.unban = async user => {
  await redis.srem('users:banned', user.id);
  refresh();
}
