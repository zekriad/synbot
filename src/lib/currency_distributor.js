const redis = require('./redis');
const auth = require('./auth');

const DISTRIBUTION_MINUTES = 6;

module.exports = class CurrencyDistributor {
  constructor(client) {
    this.client = client;
  }

  startDistribution() {
    this.client.guilds.forEach(g => {
      setInterval(() => {
        g.members.forEach(m => {
          if (this.isOnline(m)) {
            this.incrementCurrency(m);
          }
        });
      }, DISTRIBUTION_MINUTES * 60 * 1000); // every 6 minutes
    });
  }

  isOnline(user) {
    return !auth.isBanned(user) && user.presence.status === 'online';
  }

  incrementCurrency(user) {
    redis.hincrby(`users:${user.id}`, 'currency', 1);
  }
};
