const Redis = require('ioredis');

function getClient() {
  if (process.env.REDIS_URL) {
    return new Redis({ host: process.env.REDIS_URL, password: process.env.REDIS_PASSWORD });
  } else {
    console.log('Using localhost Redis with no auth.');
    return new Redis();
  }
}

const client = getClient();
module.exports = client;
