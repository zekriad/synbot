module.exports = class Matcher {
  constructor(message) {
    this.message = message;
  }

  before() {

  }

  call() {
    return new Error(`#{this.constructor.name} not implemented yet.`);
  }

  after() {

  }

  runLifecycle() {
    this.before();
    this.call();
    this.after();
  }
};

module.exports.key = null;
