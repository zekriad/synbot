const Matcher = require('./matcher');
const history = require('../support/history');
const { bold } = require('../support/text_style');

module.exports = class WhatMatcher extends Matcher {
  call() {
    this.message.channel.send(bold(history.getLast(this.message.channel.name).toUpperCase()));
  }
};

module.exports.key = "^what\\?*$";
module.exports.aliases = ["^wat\\?*$", "^wut\\?*$", "^wot\\?*$"];
