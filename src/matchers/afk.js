const Matcher = require('./matcher');
const afk = require('../support/afk');

afkers = {};

module.exports = class AFKMatcher extends Matcher {
  call() {
    afk.addAfker(this.message.author);
  }
};

module.exports.key = "^afk$";
