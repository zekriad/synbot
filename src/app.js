const Discord = require('discord.js');
const logger = require('winston');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console(), { colorize: true });
logger.level = 'debug';

// Load commands
const ActionLoader = require('./lib/action_loader');
const loader = new ActionLoader(['commands', 'matchers']);
const { commands, matchers } = loader.loadActions();
const hooks = loader.loadHooks();

const auth = require('./lib/auth');
const CurrencyDistributor = require('./lib/currency_distributor');

// Initialize Discord Bot
const bot = new Discord.Client();
bot.login(process.env.DISCORD_BOT_TOKEN);

bot.on('ready', () => {
    logger.info('Connected');
    logger.info(`Logged in as: ${bot.user.username} - (${bot.user.id})`);

    const currency = new CurrencyDistributor(bot);
    currency.startDistribution();
});

bot.on('message', message => {
  if (bot.user.id === message.author.id) return;
  if (auth.isBanned(message.author)) return;
  if (auth.isAdmin(message.author)) {
    message.author.admin = true;
  }

  const lowerContent = message.content.toLowerCase();
  const stripped = lowerContent.replace(/[^A-Z0-9]+/ig, "");

  if (lowerContent.substring(0, 1) === '!') {
    const cmd = lowerContent.split(' ')[0].substring(1);

    if (commands[cmd]) {
      const command = new commands[cmd](message);
      command.runLifecycle();
    }

  } else {
    Object.keys(matchers).forEach(key => {
      const regex = new RegExp(key, 'gi');

      if (lowerContent.match(regex)) {
        const includer = new matchers[key](message);
        includer.runLifecycle();
      }
    });
  }

  hooks.forEach(hook => (new hook(message)).runLifecycle());
});
