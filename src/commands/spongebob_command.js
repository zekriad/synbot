const Command = require('./command');
const history = require('../support/history');
const { isLetter } = require('../support/string_helpers');

module.exports = class SpongebobCommand extends Command {
  call() {
    const lastMsgCharArr = history.getLast(this.message.channel.name).toLowerCase().split('');
    const transformedCharArr = lastMsgCharArr.map(c => isLetter(c) && !!Math.round(Math.random()) ? c.toUpperCase() : c);
    this.message.channel.send(transformedCharArr.join(''));
  }
};

module.exports.key = "spongebob";
