const Command = require('./command');

module.exports = class CommandsCommand extends Command {
  call() {
    this.message.author.send(
      `\`\`\`!info
!commands
!lmgtfy [question|optional]
!remind [time|ex:5m:30s] [message]
!coin
!code8ball
!8ball
!points
!spongebob
!blackjack
!jira [number]
!hotpotato
!github [query]
!mdn [query]
!spotify [artist|album|track] [query]
!admin [add|remove] [mentioned-users]
!ban [add|remove] [mentioned-users]
!quote [add|remove] [name] [quote-to-remove]
!memory [add|remove] [remove-idx]
!currency [give] [amount] [mentioned-user]\`\`\``
    );
  }
};

module.exports.key = "commands";
