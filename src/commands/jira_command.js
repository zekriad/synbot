const Command = require('./command');

module.exports = class JiraCommand extends Command {
  call() {
    const number = this.cmdArgs[0];

    if (number) {
      this.message.channel.send(`https://jira.mediware.com/browse/MSP-${number}`);
    } else {
      this.message.channel.send("I need an issue number to link.");
    }
  }
}

module.exports.key = "jira";
