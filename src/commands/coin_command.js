const Command = require('./command');
const { sample } = require('../support/array_helpers');

module.exports = class CoinCommand extends Command {
  call() {
    this.message.channel.send(sample(["heads", "tails"]));
  }
};

module.exports.key = "coin";
