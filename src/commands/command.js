const redis = require('../lib/redis');

module.exports = class Command {
  constructor(message) {
    this.message = message;
    this.cmdArgs = message.content.split(' ').slice(1);
    this.chopped = this.cmdArgs.join(' ');
  }

  async before() {
    const author = this.message.author;
    redis.multi()
         .hset('users', author.tag, author.id)
         .hmset(`users:${author.id}`, 'name', author.username, 'tag', author.tag)
         .exec();
  }

  async call() {
    return new Error(`#{this.constructor.name} not implemented yet.`);
  }

  async after() {

  }

  async runLifecycle() {
    await this.before();
    await this.call();
    await this.after();
  }
};

module.exports.key = null;
