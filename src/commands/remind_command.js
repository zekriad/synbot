const Command = require('./command');

module.exports = class RemindCommand extends Command {
  call() {
    const calcTime = this.getTimeInMs(this.cmdArgs[0]);
    let remindMessage = this.cmdArgs.slice(1).join(' ');
    setTimeout(() => this.message.reply(`(reminder) ${remindMessage}`), calcTime);
  }

  getTimeInMs(str) {
    let timeArgs = str.split(':');
    let calcTime = 0;

    timeArgs.forEach(timeArg => {
      let number = Number(timeArg.substring(0, timeArg.length -1));
      let unit = timeArg.substring(timeArg.length - 1, timeArg.length - 0).toLowerCase();

      if(unit === 'd') {
        calcTime += number * 24 * 60 * 60 * 1000;
      } else if(unit === 'h') {
        calcTime+= number * 60 * 60 * 1000;
      } else if(unit === 'm') {
        calcTime += number * 60 * 1000;
      } else if(unit === 's') {
        calcTime += number * 1000;
      }
    });

    return calcTime;
  }
};

module.exports.key = "remind";
