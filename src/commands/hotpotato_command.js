const Command = require('./command');
const redis = require('../lib/redis');

module.exports = class HotPotatoCommand extends Command {
  async call() {
    const member = await redis.srandmember('team');
    this.message.channel.send(`Hurry! Pass it to: ${member}`);
  }
};

module.exports.key = "hotpotato";
