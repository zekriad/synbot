const Command = require('./command');
const redis = require('../lib/redis');

module.exports = class MemoryCommand extends Command {
  async call() {
    const cmd = this.cmdArgs.shift();

    switch (cmd) {
    case 'add':
      if (this.cmdArgs.length > 0) {
        await redis.rpush(`memories:${this.message.author.id}`, this.cmdArgs.join(' '));
        this.message.channel.send("I'll remember that for later.");
      } else {
        this.message.channel.send("I need something to add.");
      }
      break;

    case 'remove':
      if (this.cmdArgs.length > 0) {
        await redis.multi()
                   .lset(`memories:${this.message.author.id}`, this.cmdArgs[0], 'DELETE')
                   .lrem(`memories:${this.message.author.id}`, 1, 'DELETE')
                   .exec();
        this.message.channel.send("Consider it forgotten.");
      } else {
        this.message.channel.send("I need something to remove.");
      }
      break;

    case 'clear':
      redis.del(`memories:${this.message.author.id}`);
      this.message.channel.send("What were we talking about?");
      break;

    default:
      const memories = await redis.lrange(`memories:${this.message.author.id}`, 0, -1);
      if (memories.length > 0) {
        const list = memories.map(m => `${memories.indexOf(m)}) ${m}`);
        this.message.channel.send(list.join("\n"));
      } else {
        this.message.channel.send("I don't have any memories for you.");
      }
    }
  }
};

module.exports.key = "memory";
