const Command = require('./command');
const auth = require('../lib/auth');
const Currency = require('../models/currency');

const CURRENCY_NAME = 'gold';

module.exports = class CurrencyCommand extends Command {
  async call() {
    if (!this.message.guild) return;

    const currency = new Currency(this.message.author);
    const cmd = this.cmdArgs.shift();

    switch (cmd) {
    case 'give':
      this.giveHandler(currency);
      break;

    default:
      this.message.channel.send(`${this.message.author.username} has ${await currency.total()} ${CURRENCY_NAME}.`);
    }
  }

  async giveHandler(currency) {
    if (!this.message.mentions.members) return;

    const amount = this.cmdArgs.shift();

    let target = this.message.mentions.members.first();
    if (!target) return;
    target = target.user;


    if (amount > await currency.total()) {
      this.message.channel.send("You're too poor for that.");
      return;
    }

    if (amount && !auth.isBanned(target)) {
      currency.give(target, amount);
      this.message.channel.send(`${this.message.author.username} gives ${target.username} ${amount} ${CURRENCY_NAME}.`);
    } else {
      this.message.channel.send(`What? Try !currency give 1 @whoever.`);
    }
  }
};

module.exports.key = "currency";
