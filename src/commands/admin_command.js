const Command = require('./command');
const auth = require('../lib/auth');

module.exports = class AdminCommand extends Command {
  call() {
    if (!this.message.author.admin && auth.adminList().length > 0) return;
    if (!this.message.mentions.members) return;

    const cmd = this.cmdArgs.shift();
    let users = [];
    this.message.mentions.members.forEach(m => users.push(m.user));

    switch (cmd) {
    case 'add':
      users.forEach(user => auth.addAdmin(user));
      this.message.channel.send(`Score! You've been promoted: ${users.map(u => u.username).join(', ')}.`);
      break;

    case 'remove':
      users.forEach(user => auth.removeAdmin(user));
      this.message.channel.send(`Demotion time: ${users.map(u => u.username).join(', ')}.`);
      break;

    default:
      this.message.channel.send(`Admins: ${auth.adminList().join(', ')}`);
    }
  }
};

module.exports.key = "admin";
