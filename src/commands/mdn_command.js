const Command = require('./command');
const axios = require('axios');

module.exports = class MDNCommand extends Command {
  async call() {
    if (this.cmdArgs.length < 1) {
      this.message.channel.send('I need something to search for, chief.');
      return;
    }

    const result = await this.getDoc(this.cmdArgs.join('+'));
    this.message.channel.send(result);
  }

  async getDoc(query) {
    try {
      const response = await axios.get(`https://developer.mozilla.org/en-US/search.json?q=${query}`);

      if (response.data.documents.length > 0) {
        return response.data.documents[0].url;
      } else {
        return `I can't find anything on MDN about ${this.chopped}, boss.`;
      }
    } catch (error) {
      return "I don't feel like it. I've seen enough MDN for now.";
    }
  }
};

module.exports.key = "mdn";
