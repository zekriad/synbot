const Command = require('./command');
const redis = require('../lib/redis');
const { sample } = require('../support/array_helpers');

module.exports = class QuoteCommand extends Command {
  async call() {
    if (this.cmdArgs.length === 0) {
      this.getRandomQuote();
      return;
    }

    const subCommand = this.cmdArgs.shift();
    switch (subCommand) {
      case 'add':
        await this.addQuote(this.cmdArgs.shift(), this.cmdArgs.join(' '));
        break;

      case 'remove':
        await this.removeQuote(this.cmdArgs.shift(), this.cmdArgs.join(' '));
        break;

      default:
        await this.getRandomQuote(subCommand);
    }
  }

  async getRandomQuote(key = null) {
    let quote;

    if (key) {
      quote = await redis.srandmember(`quotes:${key}`);
    } else {
      const quoteKeys = await redis.smembers('quotes:index');
      const quotes = await redis.sunion(quoteKeys.map(key => `quotes:${key}`));
      quote = sample(quotes);
    }

    if (quote) {
      const msg = key ? `**${key}:** ${quote}` : `${quote}`;
      this.message.channel.send(msg);
    } else {
      this.message.channel.send(`I don't have any good quotes for ${key}`);
    }
  }

  async addQuote(key, quote) {
    redis.multi()
         .sadd(`quotes:${key}`, quote)
         .sadd('quotes:index', key)
         .exec();

    this.message.channel.send(`I added the quote for ${key}.`);
  }

  async removeQuote(key, quote) {
    const success = await redis.srem(`quotes:${key}`, quote);
    if (success) {
      this.message.channel.send('Consider those foolish words forgotten.');
      this.updateIndex(key);
    } else {
      this.message.channel.send(`I didn't see that quote by ${key}.`);
    }
  }

  async updateIndex(key) {
    const quotes = await redis.smembers(`quotes:${key}`);

    if (quotes.length === 0) {
      redis.srem(`quotes:index`, key);
    }
  }
};

module.exports.key = "quote";
