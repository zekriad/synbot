const Command = require('./command');
const { sample } = require('../support/array_helpers');

const fibonacci = [0, 1, 2, 3, 5, 8, 13, 21, 34];

module.exports = class PointsCommand extends Command {
  async call() {
    this.message.channel.send(sample(fibonacci));
  }
};

module.exports.key = "points";
