const Command = require('./command');
const { sample } = require('../support/array_helpers');

const messages = [
  "Yes - definitely.",
  "Most likely.",
  "Signs point to yes.",
  "Reply hazy, try again.",
  "Better not tell you now.",
  "Concentrate and ask again.",
  "Don't count on it.",
  "Cannot predict now.",
  "Very doubtful.",
  "Ask again later.",
  "Without a doubt.",
  "Outlook good.",
  "My reply is no.",
  "Yes.",
  "As I see it, yes.",
  "It is decidedly so.",
  "It is certain.",
  "You may rely on it.",
  "Outlook not so good.",
  "My sources say no."
];

module.exports = class Magic8BallCommand extends Command {
  async call() {
    this.message.channel.send(sample(messages));
  }
};

module.exports.key = "8ball";
