const Command = require('./command');
const { randomInt } = require('../support/number_helpers');

module.exports = class DiceCommand extends Command {
  call() {

    let matches = this.chopped.match(/(\d*)d(\d+)\s*([+-]\d+)?/i);
    matches = matches ? matches : [];
    let [_, num, sides, modifier] = matches.map(s => parseInt(s));

    num = num || 1;
    sides = sides || 20;
    modifier = modifier || 0;

    let value = Array(num).fill()
                          .map(_ => randomInt(1, sides))
                          .reduce((sum, e) => sum + e);
    value += modifier;

    this.message.channel.send(`_rolls ${num}d${sides}m${modifier}: **${value}**_`);
  }
};

module.exports.key = "dice";
module.exports.aliases = ["roll"];
