const Command = require('./command');
const history = require('../support/history');

module.exports = class EchoCommand extends Command {
  call() {
    this.message.channel.send(history.getLast(this.message.channel.name));
  }
};

module.exports.key = "echo";
