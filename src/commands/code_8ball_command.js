const Command = require('./command');
const { sample } = require('../support/array_helpers');

const messages = [
  "Didn't restart server",
  "The timeline doesn't allow you to fix it",
  "Invalid Closing Tag",
  "[Object object]",
  "Wrong browser",
  "Wrong component version",
  "YOU wrote it",
  "Vaadin",
  "Logic error",
  "No Unit Tests",
  "Missing semi-colon",
  "Evan broke it",
  "Chet overwrote it",
  "Missing comma",
  "Forgot to pull",
  "¯\\_(ツ)_/¯",
  "DOM Issues"
];

module.exports = class Code8BallCommand extends Command {
  async call() {
    this.message.channel.send(sample(messages));
  }
};

module.exports.key = "code8ball";
