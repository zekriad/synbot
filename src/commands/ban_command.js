const Command = require('./command');
const auth = require('../lib/auth');

module.exports = class BanCommand extends Command {
  call() {
    if (!this.message.author.admin) return;
    if (!this.message.mentions.members) return;

    const cmd = this.cmdArgs.shift();
    let users = [];
    this.message.mentions.members.forEach(m => users.push(m.user));

    switch (cmd) {
    case 'add':
      users.forEach(user => auth.ban(user));
      this.message.channel.send(`You are denounced: ${users.map(u => u.username).join(', ')}.`);
      break;

    case 'remove':
      users.forEach(user => auth.unban(user));
      this.message.channel.send(`You are welcome back: ${users.map(u => u.username).join(', ')}.`);
      break;

    default:
      this.message.channel.send(`Banlist: ${auth.banList().join(', ')}`);
    }
  }
};

module.exports.key = "ban";
