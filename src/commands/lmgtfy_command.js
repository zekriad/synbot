const Command = require('./command');

module.exports = class LMGTFYCommand extends Command {
  call() {
    this.message.channel.send(`http://lmgtfy.com/?q=${encodeURIComponent(this.chopped)}`);
  }
};

module.exports.key = "lmgtfy";
