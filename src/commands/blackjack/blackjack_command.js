const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackCommand extends Command {
  call() {
    this.message.channel.send(Messages.COMMANDS);
  }
};

module.exports.key = "blackjack";
