const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackQuitCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted) {
      this.message.channel.send(Messages.GAME_NOT_STARTED);
      return;
    }
    if (blackjackGame && blackjackGame.inProgress) {
      this.message.channel.send(Messages.GAME_ALREADY_IN_PROGRESS);
      return;
    }

    const username = this.message.author.username;
    blackjackGame.quit(username);
    
    if (blackjackGame.getNumberOfPlayers() > 0) {
      const players = blackjackGame.getPlayerNames();
      this.message.channel.send(Messages.getCurrentPlayersListMsg(players));
    } else {
      blackjackGame.end();
      this.message.channel.send(Messages.NO_MORE_PLAYERS);
    }
  }
};

module.exports.key = "quit";
