const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackStatusCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted || !blackjackGame.inProgress) {
      this.message.channel.send(Messages.GAME_NOT_IN_PROGRESS);
      return;
    }

    const playerDetails = blackjackGame.getAllPlayersCardsDetails();
    this.message.channel.send(Messages.getAllPlayersDetailsMsg(playerDetails));

    const curPlayer = blackjackGame.getCurrentPlayer();
    this.message.channel.send(Messages.getPlayerTurnMsg(curPlayer.user));
  }
};

module.exports.key = "status";
