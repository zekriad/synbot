const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackEndCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted) {
      this.message.channel.send(Messages.GAME_NOT_STARTED);
      return;
    }

    blackjackGame.end();
    this.message.channel.send(Messages.GAME_ENDED);
  }
};

module.exports.key = "end";
