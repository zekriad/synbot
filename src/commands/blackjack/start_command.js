const Command = require('../command');
const BlackjackGame = require('../../games/blackjack_game');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackStartCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted) {
      blackjackGame = new BlackjackGame();
      const username = this.message.author.username;
      this.message.channel.send(Messages.getNewGameMsg(username));
      blackjackGame.join(username);
    }
    if (blackjackGame.inProgress) {
      this.message.channel.send(Messages.GAME_ALREADY_IN_PROGRESS);
      return;
    }

    blackjackGame.start();

    const playerDetails = blackjackGame.getAllPlayersCardsDetails();
    const winners = playerDetails.filter(detail => blackjackGame.hasBlackjack(detail.user));

    if (winners.length > 0) {
      const allDetails = blackjackGame.getAllPlayersCardsDetails();
      this.message.channel.send(Messages.getAllPlayersDetailsMsg(allDetails, true));

      this.message.channel.send(Messages.getWinnerBlackjackMsg(winners.map(winner => winner.user)));
      blackjackGame.end();
    } else {
      this.message.channel.send(Messages.getAllPlayersDetailsMsg(playerDetails));

      const curPlayer = blackjackGame.getCurrentPlayer();
      this.message.channel.send(Messages.getPlayerTurnMsg(curPlayer.user));
    }
  }
};

module.exports.key = "start";
