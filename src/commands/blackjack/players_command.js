const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackPlayersCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted) {
      this.message.channel.send(Messages.GAME_NOT_STARTED);
      return;
    }

    const players = blackjackGame.getPlayerNames();
    this.message.channel.send(Messages.getCurrentPlayersListMsg(players));
  }
};

module.exports.key = "players";
