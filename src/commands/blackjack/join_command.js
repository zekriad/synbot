const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackJoinCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted) {
      this.message.channel.send(Messages.GAME_NOT_STARTED);
      return;
    }
    if (blackjackGame && blackjackGame.inProgress) {
      this.message.channel.send(Messages.GAME_ALREADY_IN_PROGRESS);
      return;
    }

    const username = this.message.author.username;
    blackjackGame.join(username);

    const players = blackjackGame.getPlayerNames();
    this.message.channel.send(Messages.getCurrentPlayersListMsg(players));
  }
};

module.exports.key = "join";
