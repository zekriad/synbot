const Command = require('../command');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackStandCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (!blackjackGame || !blackjackGame.gameStarted || !blackjackGame.inProgress) {
      this.message.channel.send(Messages.GAME_NOT_IN_PROGRESS);
      return;
    }

    const username = this.message.author.username;
    const curPlayer = blackjackGame.getCurrentPlayer();

    if (curPlayer.user !== username) return;

    blackjackGame.stand(curPlayer.user);

    const nextPlayer = blackjackGame.nextPlayer();

    if (nextPlayer.user !== Messages.DEALER_NAME) {
      this.message.channel.send(Messages.getPlayerTurnMsg(nextPlayer.user));
    } else {
      blackjackGame.dealerAI(Messages.DEALER_NAME);

      const allDetails = blackjackGame.getAllPlayersCardsDetails();
      this.message.channel.send(Messages.getAllPlayersDetailsMsg(allDetails, true));

      const winners = blackjackGame.computeWinners();
      this.message.channel.send(Messages.getWinnersMsg(winners));

      blackjackGame.end();
    }
  }
};

module.exports.key = "stand";
