const Command = require('../command');
const BlackjackGame = require('../../games/blackjack_game');
const Messages = require('../../games/blackjack_messages');

module.exports = class BlackjackInitCommand extends Command {
  call() {
    if (this.message.channel.name !== Messages.ROOM_NAME) return;
    if (blackjackGame && blackjackGame.gameStarted) {
      this.message.channel.send(Messages.GAME_ALREADY_STARTED);
      return;
    }

    const username = this.message.author.username;

    blackjackGame = new BlackjackGame();
    this.message.channel.send(Messages.getNewGameMsg(username));
    blackjackGame.join(username);
  }
};

module.exports.key = "init";
