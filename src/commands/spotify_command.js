const Command = require('./command');
const axios = require('axios');

let token = null;

module.exports = class SpotifyCommand extends Command {
  async call() {
    if (this.cmdArgs.length < 1) {
      this.message.channel.send('I need something to search for, chief.');
      return;
    }

    if (!token) await this.getToken();

    const type = this.cmdArgs.shift();
    const query = this.cmdArgs.join(' ');

    const result = await this.getMusic(type, query);
    this.message.channel.send(result);
  }

  async getMusic(type, query) {
    try {
      const instance = axios.create({ headers: { 'Authorization': `Bearer ${token}` } });
      const response = await instance.get(`https://api.spotify.com/v1/search?q=${encodeURIComponent(query)}&type=${type}`);

      if (response.data[`${type}s`].items.length > 0) {
        return response.data[`${type}s`].items[0].external_urls.spotify;
      } else {
        return `I can't find any ${type}s on Spotify named ${query}, boss.`;
      }
    } catch (error) {
      return "I don't feel like it. I've heard enough noise on Spotify for now.";
    }
  }

  async getToken() {
    const clientID = process.env.SPOTIFY_CLIENT_ID;
    const secret = process.env.SPOTIFY_CLIENT_SECRET;
    const b64 = Buffer.from(clientID + ':' + secret).toString('base64');

    try {
      const instance = axios.create({ headers: {
                                      'Authorization': `Basic ${b64}`,
                                      'Content-Type':'application/x-www-form-urlencoded'
                                    } });
      const params = new URLSearchParams();
      params.append('grant_type', 'client_credentials');
      const response = await instance.post('https://accounts.spotify.com/api/token', params);

      token = response.data.access_token;
      setTimeout(() => token = null, response.data.expires_in * 1000);
    } catch (error) {
      console.log(error);
    }
  }
};

module.exports.key = "spotify";
