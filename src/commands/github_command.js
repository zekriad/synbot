const Command = require('./command');
const axios = require('axios');

module.exports = class GithubCommand extends Command {
  async call() {
    if (this.cmdArgs.length < 1) {
      return this.message.channel.send('I need something to search for, chief.');
    }

    const result = await this.getRepo(this.cmdArgs.join('+'));
    this.message.channel.send(result);
  }

  async getRepo(query) {
    try {
      const response = await axios.get(`https://api.github.com/search/repositories?q=${query}`);

      if (response.data.items.length > 0) {
        return response.data.items[0].html_url;
      } else {
        return `I can't find anything on Github about ${this.chopped}, boss.`;
      }
    } catch (error) {
      return "I don't feel like it. I've seen enough Github for now.";
    }
  }
};

module.exports.key = "github";
